<?php

function VVaultUrl($subdomain){
	$vaultURL = "https://" . $subdomain . ".veevavault.com";
	$vaultAPI = $vaultURL . "/api/v21.1";
	return array(
		$vaultURL,
		$vaultAPI
	);
}

function getCurlError($result){
	$error = array();
	if(gettype($result) != "array") array_push($error, $result);
	else for($i = 0; $i < count($result); $i++){
		array_push($error, $result[$i] -> type . ": " . $result[$i] -> message);
	}
	return $error;
}

function curlError($data, $curl = true){
	if(!$curl || !$data) return [false, true];
	if(gettype($data -> errors) == "array") return [false, $data -> errors];
	if($data -> errorType) return [false, $data -> responseMessage];
	return [true, false];
}

function login($api, $username, $password){
	ob_start();
	// curl
	$curl = curl_init();
	// curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_URL, $api . "/auth");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/x-www-form-urlencoded",
		"Accept: application/json"
	));
	curl_setopt($curl, CURLOPT_POSTFIELDS, implode("&", array(
		"username=" . urlencode($username),
		"password=" . urlencode($password)
	)));
	// get data
	$result = curl_exec($curl);
	if(curl_errno($curl)) $result = curl_error($curl);
	curl_close($curl);
	$data = ob_get_contents();
	if($data) $data = json_decode($data);
	//
	ob_end_clean();
	// result
	[$success, $error] = curlError($data, $curl);
	return array(
		$data -> sessionId,
		$success,
		$error
	);
}

function getDocToken($api, $sessionId, $docId, $group = ""){
	if(gettype($docId) == "array") $docId = implode(",", $docId);
	else $docId = trim($docId);
	//
	ob_start();
	// curl
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $api . "/objects/documents/tokens");
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		"Authorization: Bearer " . $sessionId,
		"Content-Type: application/x-www-form-urlencoded"
	));
	curl_setopt($curl, CURLOPT_POSTFIELDS, implode("&", array(
		"docIds=" . $docId,
		"downloadOption=PDF",
		"tokenGroup=" . $group,
		"expiryDateOffset=1"
	)));
	// get data
	$result = curl_exec($curl);
	if(curl_errno($curl)) $result = curl_error($curl);
	curl_close($curl);
	$data = ob_get_contents();
	if($data) $data = json_decode($data);
	//
	ob_end_clean();
	// result
	[$success, $error] = curlError($data, $curl);
	return array(
		$data -> tokens,
		$success,
		$error
	);
}