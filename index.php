<?php
header("Content-type: text/plain; charset=UTF-8");

if(!trim($_GET['docIds'])) exit("Expected URL: ?docIds=1,2,3");

include 'vv-api.php';

// vault url
$subdomain = "vv-agency";
[$vaultURL, $vaultAPI] = VVaultUrl($subdomain);

// credentials
$username = "user@vv.com";
$password = "****";

// SESSION ID
// SESSION ID
// SESSION ID

[$SESSION_ID, $success, $error] = login($vaultAPI, $username, $password);
if(!$success) exit("LOGIN ERROR: " . implode("\n", getCurlError($error)));

// DOC TOKENS
// DOC TOKENS
// DOC TOKENS

[$DOC_TOKEN, $success, $error] = getDocToken($vaultAPI, $SESSION_ID, $_GET["docIds"]);
if(!$success) exit("TOKEN ERROR: " . implode("\n", getCurlError($error)));

$final = -1;
$errors = array();
for($i = 0; $i < count($DOC_TOKEN); $i++){
	[$success, $error] = curlError($DOC_TOKEN[$i]);
	if(!$success) array_push($errors, implode("\n", getCurlError($error)));
	else if($final < 0) $final = $i;
}

if(count($errors)) exit(implode("\n", $errors));

// APPROVED VIEWER
// APPROVED VIEWER
// APPROVED VIEWER

header("Content-type: text/html; charset=UTF-8");

for($i = 0; $i < count($DOC_TOKEN); $i++){
	?>
<div>
	<a href="<?= $vaultURL . "/ui/approved_viewer?token=" . $DOC_TOKEN[$i]->token__v ?>" target="_blank">
		<?= $vaultURL . "/ui/approved_viewer?token=" . $DOC_TOKEN[$i]->token__v ?>
	</a>
</div>
	<?php
}
?>